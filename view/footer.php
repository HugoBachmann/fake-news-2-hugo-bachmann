<footer class="container text-center">
<h3 class="mb-4">des questions ? Contactez-nous</h3>
<div class="d-flex justify-content-around">
    <div id="ftLeft" class="d-flex text-left">
        <ul>
            <li class="mb-4">1234 Ch. de la perdition 457R6 Quelque-Part Jupiter</li>
            <li class="mb-4">(0_O)\0_\0/_0/(T_T)</li>
            <li class="mb-4">ne-pas-repondre@fake-news.info</li>
        </ul>
    </div>
    <div id="ftRight" class="d-flex text-left">
        <ul>
            <li class="mb-4">@Fakenwes</li>
            <li class="mb-4">instagram.com/fake-news</li>
            <li class="mb-4">dribble.com/fake-news</li>
            <li class="mb-4">facebook.com/fake-news</li>
        </ul>
    </div>
</div>
</footer>

</body>
</html>