<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,400;0,600;1,300&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a2e60af953.js" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="../style/style.css">
    <title><?php echo $title ?></title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent ">
            <div class="collapse navbar-collapse d-flex justify-content-around" id="navbarNav">
                <ul class="navbar-nav ">
                    <li class="nav-ite d-flex">
                        <a class="nav-link hover" href="index.php"><i class="fas fa-home"></i>Rembobiner</a>
                    </li>
                    <li class="nav-item d-flex">
                       
                        <a class="nav-link hover" href="tet.php"><i class="far fa-chart-bar"></i>Truc En Toc</a>
                    </li>
                    <li class="nav-item d-flex">
                        <a class="nav-link hover" href="rouage.php"><i class="fas fa-cog"></i>Rouage</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>