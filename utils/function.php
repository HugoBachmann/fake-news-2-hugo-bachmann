<?php

require_once "../utils/constant.php";

$articleJson = file_get_contents(DB_ARTICLE);
$dataArticle = json_decode($articleJson, true);

$userJson = file_get_contents(DB_USER);
$dataUser = json_decode($userJson, true);

//permet le changement de titre onglet en fct de la page

function showTitle( $title ) : void
{
    echo "<h1>" . $title . "</h1>";
}


function isAdmin() : bool
{
    return ($_SESSION['user']['role'] == 'admin');
}

// user est co en rédacteur

function isRedac() : bool
{  // on retourne le résultat du test de connection
    return ($_SESSION['user']['role'] == 'redac');
}

