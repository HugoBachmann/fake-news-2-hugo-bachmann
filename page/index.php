<?php
    $title = 'FAKE NEWS II';
    include('../view/header.php');

    require_once "../utils/constant.php";
    require_once "../utils/function.php";

    $articleJson = file_get_contents(DB_ARTICLE);
    $dataArticle = json_decode($articleJson, true);

    
    
ini_set('display_errors','off');


?>

<main class="container">
        <div id="subHead" class="text-center">
            <h1>fake news II</h1>
            <p id="citation">Il revient et il est pas content ! <br> mythoné en php et mysql</p>
        </div>
        <div id="separation">
            <hr>
            <hr>
        </div>
        <div>
            <h2 class="text-center pb-4">Les dernières <strong>fake news</strong></h2>
        </div>
        <div class="d-flex justify-content-around" >
            <?php 

                for($a = 0 ; $a <= 2; $a++){
                    if(isAdmin(true) || isRedac(true)){
                    echo"
                    <div class='article d-flex'>
                    
                            <input type='hidden' name='index' value='" . $dataArticle[$a]['id'] ."'>
                            <div class='card' style='width: 18rem;''>
                            <div class='card-body'>
                            <img src='../img/" . $dataArticle[$a]['img'] . "'" . "class='card-img-top' alt='image'>" .
                            "<p>" . $dataArticle[$a]['date'] . "</p>" .
                            "<h5 class'card-title'>" . $dataArticle[$a]['title'] . "</h5>" .
                            "<p class='card-text'>" . $dataArticle[$a]['resume'] . "</p>" . 
                            "</div>
                            <div class='d-flex justify-content-center'>
                            <form method='POST' action='detailArticle.php' class='mx-2'>
                                <input type='hidden' name='index' value='" . $dataArticle[$a]['id'] ."'>
                                <button class='btn btn-sm btn-outline-primary'>Détails</button>
                            </form>
                            <form method='POST' action='updateArticle.php' class='mx-2'>
                                <input type='hidden' name='index' value='" . $dataArticle[$a]['id'] ."'>
                                <button class='btn btn-sm btn-outline-success'>Editer</button>
                            </form>
                            <form method='POST' action='rouage.php' class='mx-2'>
                                <input type='hidden' name='indexArt' value='" . $dataArticle[$a]['id'] ."'>
                                <input type='hidden' name='deleteArt' value='" . $dataArticle[$a]['id'] ."'>
                                <button class='btn btn-sm btn-outline-danger mb-2'>Supprimer</button>
                            </form>
                            </div>
                         </div>
                         </div>";
                    }else{
                        echo "
                        <div class='article d-flex'>
                            <div class='card' style='width: 18rem;''>
                            <div class='card-body'>
                            <img src='../img/" . $dataArticle[$a]['img'] . "'" . "class='card-img-top' alt='image'>" .
                            "<p>" . $dataArticle[$a]['date'] . "</p>" .
                            "<h5 class'card-title'>" . $dataArticle[$a]['title'] . "</h5>" .
                            "<p class='card-text'>" . $dataArticle[$a]['resume'] . "</p>" . 
                            "</div>
                            <form method='POST' action='detailArticle.php' class='mx-2 d-flex justify-content-center'>
                                <input type='hidden' name='index' value='" . $dataArticle[$a]['id'] ."'>
                                <button class='btn btn-sm btn-outline-primary mb-2'>Détails</button>
                            </form>
                        </div></div>"; 
                    }
                }
            ?>
            
        </div>
            <div class="d-flex justify-content-around mt-4">
                <form action="tet.php" method="get">
                    <button class="btn btn-secondary">J'en veux encore !</button>
                </form>
            </div>
        <div id="citation">
            <p id="pcitation" class="text-center">"on peut tromper une fois mille personnes, mais on ne peut pas tromper mille une personne." - émile</p>
        </div>
    </main>



<?php
    include('../view/footer.php');
?>