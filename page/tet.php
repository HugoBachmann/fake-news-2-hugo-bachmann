<?php
    $title = 'FAKE NEWS II';
    include('../view/header.php');

    require_once "../utils/constant.php";
    require_once "../utils/function.php";

    $articleJson = file_get_contents(DB_ARTICLE);
    $dataArticle = json_decode($articleJson, true); 
    
    if( isset( $_REQUEST['deleteArticle']) ) {
        array_splice($dataArticle, $_REQUEST['indexArticle'], 1);
        file_put_contents("../data/article.json", json_encode($dataArticle));
        echo "<meta http-equiv='refresh' content='0'>";
    }

?>


<main class="container pb-4">
        <div id="subHead" class="text-center">
            <h1>Trucs En toc</h1>
            <p id="citation">mais pusqu'on vous dit que c'est vrai ! </p>
        </div>
        <div id="separation">
            <hr>
            <hr>
        </div>
        <div class="d-flex justify-content-around" >

        <?php

for($a = 0 ; $a <= count($dataArticle) - 1; $a++){
    
    if(isAdmin(true) || isRedac(true)){
        echo"<div class='media mt-4'>
        <img src='../img/" . $dataArticle[$a]['img'] . "'" . "class='align-self-start mr-3' alt=''>
        <div class='media-body'>
        <p>" . $dataArticle[$a]['date'] . "</p>
        <h5 class='mt-0'>" . $dataArticle[$a]['title'] . "</h5>
        <p>" . $dataArticle[$a]['resume'] . "</p>
        </div>
        <form method='POST' action='detailArticle.php' class='mx-2'>
            <input type='hidden' name='index' value='" . $dataArticle[$a]['title'] ."'>
            <button class='btn btn-sm btn-outline-primary'>Détails</button>
        </form>
        <form method='POST' action='updateArticle.php' class='mx-2'>
            <input type='hidden' name='index' value='" . $dataArticle[$a]['title'] ."'>
            <button class='btn btn-sm btn-outline-success'>Editer</button>
        </form>
        <form method='POST' action='tet.php' class='mx-2'>
            <input type='hidden' name='indexArticle' value='" . $dataArticle[$a]['id'] ."'>
            <input type='hidden' name='deleteArticle' value=''>
            <button class='btn btn-sm btn-outline-danger'>Supprimer</button>
        </form>
        </div>
        <div id='separation1'>
        <hr>
        </div>
         </div>
   ";
    }else{
        echo"
        <div class='media mt-4'>
            <img src='../img/" . $dataArticle[$a]['img'] . "'" . "class='align-self-start mr-3' alt=''>
            <div class='media-body'>
            <p>" . $dataArticle[$a]['date'] . "</p>
            <h5 class='mt-0'>" . $dataArticle[$a]['title'] . "</h5>
            <p>" . $dataArticle[$a]['resume'] . "</p>
            </div>
            <form method='POST' action='detailArticle.php' class='mx-2'>
                <input type='hidden' name='index' value='" . $dataArticle[$a]['id'] ."'>
                <button class='btn btn-sm btn-outline-primary'>Détails</button>
            </form>
            </div>
            <div id='separation1'>
            <hr>
            </div>
       </div>
       ";
    }
}
?>
</main>

<?php
    include('../view/footer.php');
?>
    











