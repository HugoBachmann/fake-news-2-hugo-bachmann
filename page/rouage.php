<?php 

$title = 'ROUAGE';
include('../view/header.php');
require_once "../utils/constant.php";
require_once "../utils/function.php";




$articleJson = file_get_contents(DB_ARTICLE);
$dataArticle = json_decode($articleJson, true);

$userJson = file_get_contents(DB_USER);
$dataUser = json_decode($userJson, true);



// ajoute d'un compte rédacteur 
$date = new DateTime();

if(isset($_POST['login'])){
   
    if($_POST['CSRF'] == md5($date->format('Y-m-d').CSRF)) {

        $user = [];
        $user['id'] = count($dataUser) == 0 ? 1 : $dataUser[count($dataUser) -1]['id'] +1;
        $user['login'] = $_POST['login'];
        $user['password'] = md5($_POST['password']. CSRF);
        $user['role'] = $_POST['role'];


    }
    array_push($dataUser, $user);
    file_put_contents(DB_USER, json_encode($dataUser));
    echo "<meta http-equiv='refresh' content='0'>";
    echo "utilisateur ajouté avec succès";
}

//delete user

if( isset( $_REQUEST['deleteUser']) ) {
    array_splice($dataUser, $_REQUEST['index'], 1);
    file_put_contents("../data/user.json", json_encode($dataUser));
}

//delete article

if( isset( $_REQUEST['deleteArt']) ) {
    array_splice($dataArticle, $_REQUEST['indexArt'], 1);
    file_put_contents("../data/article.json", json_encode($dataArticle));
}

//connection 

if(isset($_REQUEST['loginForm']) &&  $_REQUEST['loginForm'] == 1){ //recup le form connection
    $testUser = false;
        foreach($dataUser as $user){
            if(($_POST['login'] == $user['login'])){
                if($user['password'] == md5($_POST['password']. CSRF)){
                        $_SESSION['conected'] = true;
                        $_SESSION['user'] = $user;
                        $testUser = true;
                        
                }else{
                    echo "mauvais indentifiant ou mot de passe";
                }
            }
        }
    }


// déconnexion



if(isset($_POST['logout'])){ // gestion de la déconnexion
    if(isset($_SESSION['conected']) && $_SESSION['conected']){
        $_SESSION = [];
        session_destroy();
    }
}

//creation d'article

if(isset($_POST['artTitle'])){
   
    
        $article = [];
        $article['id'] = count($dataArticle) == 0 ? 1 : $dataArticle[count($dataArticle) -1]['id'] +1;
        $article['date'] = $_POST['artDate'];
        $article['title'] = ($_POST['artTitle']);
        $article['resume'] = $_POST['artResume'];
        $article['content'] = $_POST['artContent'];
        $article['img'] = $_POST['artImg'];


    array_push($dataArticle, $article);
    file_put_contents(DB_ARTICLE, json_encode($dataArticle));
    echo "<meta http-equiv='refresh' content='0'>";
    echo "article créer avec succès";
}

 
if(isAdmin(true)){ // affiche les options administrateur

//btn deconnexion
    echo  '<form action="rouage.php" method="post" class="btndeco"><input class="btn btn-sm btn-outline-danger" name="logout" type="submit" value="Se déconnecter"></form>'; 
    
//form création de compte rédacteur
    echo '<div class="container text-center mt-4">
    <h2>Création de comptes rédacteurs</h2>
    <form method="POST" action="rouage.php">
         <input class="mt-2  connect form-control text-center" type="text" name="login" placeholder="Nom d\'utilisateur" required>
         <input class="mt-2  connect form-control text-center" type="password" name="password" placeholder="Mot de passe du rédacteur" required>
         <input class="mt-2  connect form-control text-center" type="hidden" name="role" value="redac" required>
         <input class="mt-2  connect form-control text-center" type="hidden" name="page" value="rouage">
         <input class="mt-2  connect form-control text-center" type="hidden" name="CSRF" value="'.  md5($date->format('Y-m-d').CSRF) .'">
         <div class="form-example"><input class=" mt-2 btn btn-sm btn-outline-success" type="submit" value="Créer"></div>
    </form>
</div>
<div class="container text-center mt-4">
';

//form création article
    echo'
    <h2>Création d\'article</h2>
     <form action="rouage.php" method="POST">
         <input class="mt-2  connect form-control text-center" type="date" name="artDate" placeholder="Date de l\'artcile"  required>
         <input class="mt-2  connect form-control text-center" type="text" name="artTitle" placeholder="Titre de l\'article"  required>
         <input class="mt-2  connect form-control text-center" type="textarea" name="artResume" placeholder="Résumer de l\'article"  required>
         <input class="mt-2  connect form-control text-center" type="textarea" name="artContent" placeholder="Contenu"  required>
         <input class="mt-2  connect form-control text-center" type="text" name="artImg" placeholder="image"  required>
         <input class="mt-2  connect form-control text-center" type="hidden" name="page" value="rouage"
         <div class="form-example"><input class=" mt-2 btn btn-sm btn-outline-success" type="submit" value="Créer"></div>
     </form>
</div>';

 //affiche la liste des rédacteur
 
echo'<div class="container text-center mt-4">
        <h2>Gestion des rédacteurs</h2>
        <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Login</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>';
        for($i = 0 ; $i <= count($dataUser)-1; $i++){
            if($dataUser[$i]['role'] == 'redac'){
                echo "
                <tr>          
                  <td>" .  $dataUser[$i]['id'] . "</td>
                  <td>" .  $dataUser[$i]['login'] . "</td>
                  <td class='d-flex justify-content-center'>
                      <form method='POST' action='updateUser.php' class='mx-2'>
                          <input type='hidden' name='index' value='" . $dataUser[$i]['id'] ."'>
                          <button class='btn btn-sm btn-outline-success'>Editer</button>
                      </form>
                      <form method='POST' action='rouage.php' class='mx-2'>
                          <input type='hidden' name='index' value='" . $dataUser[$i]['id'] ."'>
                          <input type='hidden' name='deleteUser' value=''>
                          <button class='btn btn-sm btn-outline-danger'>Supprimer</button>
                      </form>
                  </td>";
            }
        }
echo'</tbody>
</table>';

    
     

 } else if(isRedac(true)){ // affiche les options Rédacteur

    //btn de déconnexion 
     echo  '<form action="rouage.php" method="post" class="btndeco"><input class="btn btn-sm btn-outline-danger" name="logout" type="submit" value="Se déconnecter"></form>'; 
     
    //form de création d'article
     echo'
     <div class="container text-center">
     <h2>Création d\'article</h2>
     <form action="rouage.php" method="POST">
         <input class="mt-2  connect form-control text-center" type="date" name="artDate" placeholder="Date de l\'artcile"  required>
         <input class="mt-2  connect form-control text-center" type="text" name="artTitle" placeholder="Titre de l\'article"  required>
         <input class="mt-2  connect form-control text-center" type="textarea" name="artResume" placeholder="Résumer de l\'article"  required>
         <input class="mt-2  connect form-control text-center" type="textarea" name="artContent" placeholder="Contenu"  required>
         <input class="mt-2  connect form-control text-center" type="text" name="artImg" placeholder="image"  required>
         <input class="mt-2  connect form-control text-center" type="hidden" name="page" value="rouage"
         <div class="form-example"><input class=" mt-2 btn btn-sm btn-outline-success" type="submit" value="Créer"></div>
     </form>
</div>';
 
 }else{  // affiche le formulaire de connection
     echo '
     <div class="container text-center mt-4">
         <h2>Connection</h2>
         <form action="rouage.php" method="POST">
             <div class="form-group">
                 <input class="mt-2   connect form-control text-center" type="text" name="login"  placeholder="Nom d\'utilisateur" required>
             </div>
             <div class="form-group">
                 <input class="" type="hidden" name="loginForm" value="' . 1 . '" required>
             </div>
             <div class="form-group">
                 <input class="mt-2   connect form-control text-center" type="password" name="password" placeholder="Mot de passe" required>
             </div>
             <div class="form-example mt-2">
                 <input class="btn btn-sm btn-outline-primary" type="submit" value="Se connecter">
             </div>
         </form>
     </div>
     ';
 }
?>



        