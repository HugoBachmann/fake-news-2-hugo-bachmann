<?php
$title = 'FAKE NEWS II';
include('../view/header.php');
require_once "../utils/constant.php";
require_once "../utils/function.php";

$articleJson = file_get_contents(DB_ARTICLE);
$dataArticle = json_decode($articleJson, true);



$art = $dataArticle[$_REQUEST['index']];


?>

<div class="container">
    <div class="text-center">
        <h1><?php echo $art["title"]; ?></h1>
    </div>
    <div class="text-center">
        <p class="date"><?php echo $art["date"]; ?></p>
    </div>
    <div class="text-center">
        <figure>
            <img src="../img/<?php echo $art["img"] ?>" alt="">
        </figure>
    </div>
    <div>
        <h4>Résumé</h4>
        <p><?php echo $art["resume"]; ?></p>
    </div>
    <div>
        <h4>Contenu</h4> 
        <p><?php echo $art["content"]; ?></p>
    </div>
</div>